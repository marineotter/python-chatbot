from django.core.management.base import BaseCommand

from ...models import QuestionExample, Answer, AiCoreQueuedTask
from aicore.botcore import BotModel

import pandas as pd
from aicore.common import logger, MecabInterface, Word2vecInterface
from time import sleep
import threading
import os
import datetime
import pytz
from django.conf import settings

import sys

sys.path.append(settings.BASE_DIR)


class Command(BaseCommand):
    # python manage.py help count_entryで表示されるメッセージ
    help = ''
    train_thread = None
    train_task_id = None

    def __init__(self):
        self.logger = logger.getChild(self.__class__.__name__)

    # コマンドライン引数を指定します。(argparseモジュール https://docs.python.org/2.7/library/argparse.html)
    def add_arguments(self, parser):
        parser.add_argument('-e', '--max_epoch', default=500, type=int)
        parser.add_argument('-b', '--batch_size', default=5, type=int)

    def train_all(self, task: AiCoreQueuedTask, model_filepath: str):
        temp_model = BotModel(self.mecab, self.w2v, model_filepath)

        self.prepare_train_data()
        temp_model.load(
            self.train_x_df, self.train_y_df,
            epoch=self.max_epoch, batch_size=self.batch_size,
            reset=True, autotrain=True, autosave=True
        )
        self.logger.info("Train task completed.")

    def exec_task(self, task: AiCoreQueuedTask, model: BotModel):
        if task.task_type == AiCoreQueuedTask.TaskType.TRAINALL:
            if self.train_thread and self.train_thread.is_alive():
                # すでにスレッドが走ってたら何もしないで一旦戻る。
                return
            else:
                task.started_at = datetime.datetime.now(
                    pytz.timezone(settings.TIME_ZONE)
                )
                task.save()

                self.logger.info("Start training.")

                self.train_thread = threading.Thread(
                    target=self.train_all, args=(task, model.model_filename,))
                self.train_thread.start()

                self.train_task_id = task.id

                self.logger.info("Complete training.")

        elif task.task_type == AiCoreQueuedTask.TaskType.PREDICT:
            task.started_at = datetime.datetime.now(
                pytz.timezone(settings.TIME_ZONE)
            )
            task.save()
            self.logger.info("Start prediction.")

            task.content_output = model.predict(task.content_input)

            task.completed_at = datetime.datetime.now(
                pytz.timezone(settings.TIME_ZONE)
            )
            task.save()

            self.logger.info("Complete prediction.")

    # コマンドが実行された際に呼ばれるメソッド

    def prepare_train_data(self):
        train_x_df_middle_dict = {"content": [], "answer_index": []}
        for item in QuestionExample.objects.all():
            train_x_df_middle_dict["content"].append(item.content)
            train_x_df_middle_dict["answer_index"].append(item.answer.id)
        train_y_df_middle_dict = {"index": [], "content": []}
        for item in Answer.objects.all():
            train_y_df_middle_dict["content"].append(item.content)
            train_y_df_middle_dict["index"].append(item.id)

        self.train_x_df = pd.DataFrame.from_dict(train_x_df_middle_dict)
        self.train_y_df = pd.DataFrame.from_dict(
            train_y_df_middle_dict)

    def handle(self, *args, **options):
        self.mecab = MecabInterface()
        self.w2v = Word2vecInterface()
        self.max_epoch = options["max_epoch"]
        self.batch_size = options["batch_size"]

        model = BotModel(
            self.mecab, self.w2v,
            os.path.join(settings.BASE_DIR, "predictor.torch"))

        self.prepare_train_data()

        is_model_loaded = \
            model.load(
                self.train_x_df, self.train_y_df,
                epoch=self.max_epoch, batch_size=self.batch_size,
                reset=False, autotrain=True, autosave=True
            )

        if is_model_loaded:
            self.logger.info("Ready to predict.")
        else:
            self.logger.warn("Failed: model loading.")

        while True:
            if self.train_thread and not self.train_thread.is_alive():
                if self.train_task_id:
                    is_model_loaded = \
                        model.load(
                            self.train_x_df, self.train_y_df,
                            epoch=self.max_epoch, batch_size=self.batch_size,
                            reset=False, autotrain=False, autosave=False
                        )

                    if is_model_loaded:
                        self.logger.info("Ready to predict.")
                    else:
                        self.logger.warn("Failed: model loading.")

                    try:
                        task = AiCoreQueuedTask.objects.get(
                            id=self.train_task_id)
                        task.completed_at = datetime.datetime.now(
                            pytz.timezone(settings.TIME_ZONE)
                        )
                        task.save()
                        self.logger.info("Training task complete proc OK")
                    except Exception:
                        self.logger.warn(
                            "Training task complete proc unsuccessfully.")
                        pass
                    self.train_task_id = None

            task_query = AiCoreQueuedTask.objects.filter(
                started_at=None
            ).filter(
                completed_at=None
            ).order_by('-created_at')

            for task in task_query:
                self.exec_task(task, model)

            sleep(1)
