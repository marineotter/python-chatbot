from django.apps import AppConfig


class StoraifConfig(AppConfig):
    name = 'storaif'
