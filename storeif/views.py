from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from .models import QuestionExample, Answer, AiCoreQueuedTask

from django.views.generic import ListView, DetailView, TemplateView, View
from django.views.generic.edit import CreateView
from django.urls import reverse

from django.shortcuts import get_object_or_404
import time
import os

from django.conf import settings
import mimetypes
from django.core.files import File
from django.http import HttpResponse


class PredictView(CreateView):
    model = AiCoreQueuedTask
    template_name = "predict.html"
    fields = ("content_input",)

    def form_valid(self, form):
        form.instance.task_type = AiCoreQueuedTask.TaskType.PREDICT
        return super(PredictView, self).form_valid(form)

    def get_success_url(self):
        return reverse('predictresult', args=(self.object.id,))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["result"] = None
        args = self.request.resolver_match.kwargs
        if "pk" in args:
            try:
                for i in range(30):
                    context["result"] = self.model.objects.get(pk=args["pk"])
                    if context["result"].is_completed():
                        break
                    time.sleep(1.0)
            except Exception:
                pass

        return context


class PredictResultView(TemplateView):
    model = AiCoreQueuedTask
    template_name = "predictresult.html"

    def get_context_data(self, **kwargs):
        for i in range(30):
            queryresult = get_object_or_404(self.model, pk=kwargs["pk"])
            context = {"object": queryresult}
            if queryresult.is_completed():
                break
            time.sleep(1.0)
        return context


class DataManagementView(CreateView):
    model = AiCoreQueuedTask
    fields = tuple()

    template_name = "management.html"

    def get_context_data(self):
        context = {}
        context['histories'] = AiCoreQueuedTask.objects.filter(
            task_type=AiCoreQueuedTask.TaskType.PREDICT
        ).order_by('-created_at').all()
        last_train_task = None
        try:
            last_train_task = \
                AiCoreQueuedTask.objects.filter(
                    task_type=AiCoreQueuedTask.TaskType.TRAINALL
                ).order_by('-created_at')[0]
            if last_train_task.is_completed():
                status = "学習に成功しました。"
            else:
                status = "学習しています。\n{}".format(last_train_task.started_at)
        except Exception:
            status = "一度も学習していません。"

        last_elapsed_time = None
        try:
            for task in AiCoreQueuedTask.objects.filter(
                task_type=AiCoreQueuedTask.TaskType.TRAINALL
            ).order_by('-created_at'):
                if task.is_completed():
                    last_elapsed_time = \
                        task.completed_at - task.started_at
                    break
        except Exception:
            last_elapsed_time = None

        context['informations'] = {
            "status": status,
            "last_elapsed_time": last_elapsed_time,
            "traindatasize": QuestionExample.objects.all().count(),
            "answers": Answer.objects.all()
        }
        return context

    def form_valid(self, form):
        form.instance.task_type = AiCoreQueuedTask.TaskType.TRAINALL
        return super(DataManagementView, self).form_valid(form)

    def get_success_url(self):
        return reverse('management')


class AddAnswersView(CreateView):
    model = Answer
    template_name = "addanswer.html"
    fields = ("content",)

    def get_success_url(self):
        return reverse('management')


class LabelingView(CreateView):
    model = QuestionExample
    template_name = "labeling.html"
    fields = ("content", "answer")

    def get_initial(self, *args, **kwargs):
        try:
            obj = AiCoreQueuedTask.objects.get(pk=self.request.GET['task'])
            return {"content": obj.content_input}
        except KeyError:
            return {"content": ""}
        except ObjectDoesNotExist:
            return {"content": ""}

    def get_success_url(self):
        return reverse('management')


class LastLossImageView(View):

    def get(self, *args, **kwargs):
        target_file_path = os.path.join(settings.BASE_DIR, "losses.png")
        if not os.path.isfile(target_file_path):
            target_file_path = os.path.join(settings.REACT_HOME, "index.html")
        return HttpResponse(
            File(open(target_file_path, 'rb')),
            content_type=mimetypes.guess_type(target_file_path)[0])
