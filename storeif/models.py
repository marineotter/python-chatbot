from django.db import models
from django.utils.translation import gettext_lazy

# Create your models here.


class Answer(models.Model):
    content = models.TextField(unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"[{self.id}] {self.content}"


class QuestionExample(models.Model):
    content = models.TextField(unique=True)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    def is_trained(self):
        for item in AiCoreQueuedTask.objects.filter(
            task_type=AiCoreQueuedTask.TaskType.TRAINALL
        ).order_by('-created_at'):
            if item.is_completed():
                return self.created_at < item.started_at
        return False

    def __str__(self):
        return f"{self.content} -> {self.answer}"


class AiCoreQueuedTask(models.Model):
    class TaskType(models.TextChoices):
        TRAINALL = 'TRAINALL', gettext_lazy('AIをリセットして全データで学習')
        PREDICT = 'PREDICT', gettext_lazy('回答予測タスク')

    task_type = models.CharField(max_length=16, choices=TaskType.choices)

    created_at = models.DateTimeField(auto_now_add=True)
    started_at = models.DateTimeField(null=True, blank=True, default=None)
    completed_at = models.DateTimeField(null=True, blank=True, default=None)

    content_input = models.TextField(null=True, blank=True)
    content_output = models.TextField(null=True, blank=True)

    def is_labeled(self):
        query_for_labeled_qe = QuestionExample.objects.filter(
            content=self.content_input)
        return query_for_labeled_qe.all().count() > 0

    def is_certified(self):
        if self.is_labeled():
            labeled_answer = self.get_labeled_question_example().answer.content
            return labeled_answer == self.content_output
        else:
            return False

    def get_labeled_question_example(self):
        query_for_labeled_qe = QuestionExample.objects.filter(
            content=self.content_input)
        if self.is_labeled():
            return query_for_labeled_qe.all()[0]
        else:
            return None

    def is_completed(self):
        return self.completed_at is not None

    def __str__(self):
        return f"[{self.created_at}] {self.task_type}"
