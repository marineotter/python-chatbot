from django.contrib import admin
from .models import Answer, QuestionExample, AiCoreQueuedTask
# Register your models here.
admin.site.register(Answer)
admin.site.register(QuestionExample)
admin.site.register(AiCoreQueuedTask)
