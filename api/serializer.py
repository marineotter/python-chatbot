from rest_framework import serializers

from storeif.models import AiCoreQueuedTask, Answer, QuestionExample


class PredictionSerializer(serializers.ModelSerializer):
    is_completed = serializers.SerializerMethodField()

    # 以下4つはReadOnlyにするためにSerializerMethodFieldを使用している。
    # ReadOnlyFieldを使わなかった理由は、sourceと同名のfieldを生成することができなかったため。
    # 別名でも良かったが、別名にするほうが可読性を損ねると判断したため、同名で実装する。
    created_at = serializers.SerializerMethodField()
    started_at = serializers.SerializerMethodField()
    completed_at = serializers.SerializerMethodField()
    content_output = serializers.SerializerMethodField()

    class Meta:
        model = AiCoreQueuedTask
        fields = (
            "id", "is_completed",
            "created_at", "started_at", "completed_at",
            "content_input", "content_output")

    def get_is_completed(self, obj):
        return obj.is_completed()

    def get_content_output(self, obj):
        return obj.content_output

    def get_created_at(self, obj):
        return obj.created_at

    def get_started_at(self, obj):
        return obj.started_at

    def get_completed_at(self, obj):
        return obj.completed_at


class TrainingSerializer(serializers.ModelSerializer):
    is_completed = serializers.SerializerMethodField()

    # 以下3つはReadOnlyにするためにSerializerMethodFieldを使用している。
    # ReadOnlyFieldを使わなかった理由は、sourceと同名のfieldを生成することができなかったため。
    # 別名でも良かったが、別名にするほうが可読性を損ねると判断したため、同名で実装する。
    created_at = serializers.SerializerMethodField()
    started_at = serializers.SerializerMethodField()
    completed_at = serializers.SerializerMethodField()

    class Meta:
        model = AiCoreQueuedTask
        fields = (
            "id", "is_completed",
            "created_at", "started_at", "completed_at")

    def get_is_completed(self, obj):
        return obj.is_completed()

    def get_created_at(self, obj):
        return obj.created_at

    def get_started_at(self, obj):
        return obj.started_at

    def get_completed_at(self, obj):
        return obj.completed_at


class QuestionExampleSerializer(serializers.ModelSerializer):
    created_at = serializers.SerializerMethodField()

    class Meta:
        model = QuestionExample
        fields = ("id", "created_at", "content", "answer")

    def get_created_at(self, obj):
        return obj.created_at


class AnswerSerializer(serializers.ModelSerializer):
    created_at = serializers.SerializerMethodField()
    question_examples = serializers.SerializerMethodField()

    class Meta:
        model = Answer
        fields = (
            "id", "created_at", "content", "question_examples")

    def get_question_examples(self, obj):
        return QuestionExampleSerializer(
            QuestionExample.objects.filter(answer=obj), many=True
        ).data

    def get_created_at(self, obj):
        return obj.created_at
