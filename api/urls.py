from rest_framework import routers
from .views import PredictionViewSet, TrainingViewSet, AnswerViewSet
from .views import QuestionExampleViewSet


router = routers.DefaultRouter()
router.register(r'predictions', PredictionViewSet, r'predictions_api')
router.register(r'trainings', TrainingViewSet, r'trainings_api')
router.register(r'answers', AnswerViewSet, r'answers_api')
router.register(r'question-examples', QuestionExampleViewSet,
                r'question_examples_api')
urlpatterns = router.urls
