from django.shortcuts import render
from rest_framework import viewsets, filters
from storeif.models import AiCoreQueuedTask, Answer, QuestionExample
from .serializer import PredictionSerializer, TrainingSerializer
from .serializer import AnswerSerializer, QuestionExampleSerializer
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
import time

# Create your views here.

PREDICT_TIMEOUT_SEC = 60


class PredictionViewSet(viewsets.ViewSet):
    queryset = AiCoreQueuedTask.objects.filter(
        task_type=AiCoreQueuedTask.TaskType.PREDICT
    ).order_by("-id")
    serializer_class = PredictionSerializer

    def list(self, request):
        return Response(
            self.serializer_class(self.queryset.all(), many=True).data
        )

    def create(self, request):
        obj = AiCoreQueuedTask(
            task_type=AiCoreQueuedTask.TaskType.PREDICT,
            content_input=request.data["content_input"]
        )
        obj.save()
        obj_id = obj.id
        for i in range(PREDICT_TIMEOUT_SEC):
            obj = self.queryset.get(id=obj_id)
            if obj.is_completed():
                break
            time.sleep(1.0)
        return Response(self.serializer_class(obj).data)

    def retrieve(self, request, pk=None):
        obj = get_object_or_404(self.queryset, pk=pk)
        return Response(self.serializer_class(obj).data)


class TrainingViewSet(viewsets.ViewSet):
    queryset = AiCoreQueuedTask.objects.filter(
        task_type=AiCoreQueuedTask.TaskType.TRAINALL
    ).order_by("-id")
    serializer_class = TrainingSerializer

    def list(self, request):
        return Response(self.serializer_class(self.queryset.all(), many=True).data)

    def create(self, request):
        obj = AiCoreQueuedTask(
            task_type=AiCoreQueuedTask.TaskType.TRAINALL
        )
        obj.save()
        return Response(self.serializer_class(obj).data)

    def retrieve(self, request, pk=None):
        obj = get_object_or_404(self.queryset, pk=pk)
        return Response(self.serializer_class(obj).data)


class AnswerViewSet(viewsets.ViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer

    def list(self, request):
        return Response(
            self.serializer_class(self.queryset.all(), many=True).data
        )

    def create(self, request):
        obj = Answer(
            content=request.data["content"]
        )
        obj.save()
        return Response(self.serializer_class(obj).data)

    def retrieve(self, request, pk=None):
        obj = get_object_or_404(self.queryset, pk=pk)
        return Response(self.serializer_class(obj).data)


class QuestionExampleViewSet(viewsets.ViewSet):
    queryset = QuestionExample.objects.all()
    serializer_class = QuestionExampleSerializer

    def list(self, request):
        return Response(
            self.serializer_class(self.queryset.all(), many=True).data
        )

    def create(self, request):
        obj = QuestionExample(
            content=request.data["content"],
            answer=Answer.objects.get(id=request.data["answer"]),
        )
        obj.save()
        return Response(self.serializer_class(obj).data)

    def retrieve(self, request, pk=None):
        obj = get_object_or_404(self.queryset, pk=pk)
        return Response(self.serializer_class(obj).data)
