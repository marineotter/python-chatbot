"""chatbot URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, reverse
from django.conf.urls import include
from storeif.views import AddAnswersView, LabelingView, DataManagementView, LastLossImageView
from storeif.views import PredictView
from api.urls import urlpatterns as api_urlpatterns

urlpatterns = [
    path('', PredictView.as_view(), name='predict'),
    path('result/<int:pk>', PredictView.as_view(), name='predictresult'),
    path('data/', DataManagementView.as_view(), name='management'),
    path('lastloss/', LastLossImageView.as_view(), name='lastloss'),
    path(
        'addanswer/', AddAnswersView.as_view(),
        name='addanswer'),
    path('predict/', PredictView.as_view(), name='predict'),
    path(
        'labeling/',
        LabelingView.as_view(),
        name='labeling'),
    path('admin/', admin.site.urls),
    path('api/', include((api_urlpatterns, 'api')), name='api'),
]
