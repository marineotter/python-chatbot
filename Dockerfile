# Python support can be specified down to the minor or micro version
# (e.g. 3.6 or 3.6.3).
# OS Support also exists for jessie & stretch (slim and full).
# See https://hub.docker.com/r/library/python/ for all supported Python
# tags from Docker Hub.
FROM pytorch/pytorch:latest

# If you prefer miniconda:
#FROM continuumio/miniconda3

LABEL Name=python-chatbot Version=0.0.1

# for mecab
RUN apt-get update && \
    apt-get install locales && \
    apt-get -y install mecab libmecab-dev mecab-ipadic-utf8 git make curl xz-utils file sudo

RUN echo "ja_JP UTF-8" > /etc/locale.gen && \
    locale-gen && \
    update-locale LANG=ja_JP.UTF-8

RUN git clone --depth 1 https://github.com/neologd/mecab-ipadic-neologd.git\
    && cd mecab-ipadic-neologd\
    && bin/install-mecab-ipadic-neologd -n -y

# for debugging
RUN pip --no-cache-dir install ptvsd

# for application
ADD requirements.txt /app/requirements.txt
WORKDIR /app

# Using pip:
# その他の依存関係を入れる
RUN pip install -r requirements.txt

ENV LANG="ja_JP.UTF-8"

ADD . /app
RUN python3 manage.py makemigrations &\
    python3 manage.py migrate

EXPOSE 8000

CMD ["/bin/sh", "launch.sh"]

# Using pipenv:
#RUN python3 -m pip install pipenv
#RUN pipenv install --ignore-pipfile
#CMD ["pipenv", "run", "python3", "-m", "python-chatbot"]

# Using miniconda (make sure to replace 'myenv' w/ your environment name):
#RUN conda env create -f environment.yml
#CMD /bin/bash -c "source activate myenv && python3 -m python-chatbot"
