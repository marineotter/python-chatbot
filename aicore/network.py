import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim import SGD
import math
import numpy as np
from torch.nn.utils.rnn import pad_sequence
from torch.nn.utils.rnn import pack_padded_sequence
from torch.nn.utils.rnn import pad_packed_sequence

from .common import logger

# 参考文献：https://qiita.com/m__k/items/841950a57a0d7ff05506


class Predictor(nn.Module):
    # input ->
    # [LSTM] -> [LSTM] -> [全結合層] -> [全結合層]
    # -> output
    # 入力側の全結合層は特徴抽出を行う。（結果的には次元圧縮）
    # 出力側の全結合層は特徴展開を行う。（次元数はあまり変えない）
    def __init__(self, wordvect_size, tagset_size):
        super(Predictor, self).__init__()

        self.logger = logger.getChild(self.__class__.__name__)
        self.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu"
        )

        self.logger.info("Init: Device={}".format(self.device))

        self.input_size = wordvect_size
        self.dim_lstm1_layer = 50
        self.dim_lstm2_layer = 20
        self.dim_output1_layer = 20
        self.output_size = tagset_size

        self.input_dropout_rate = 0.3
        self.output_dropout_rate = 0.3
        self.padding_idx = 0

        # LSTM層。
        self.lstm1 = nn.LSTM(
            self.input_size, self.dim_lstm1_layer,
            batch_first=True
        )
        self.lstm2 = nn.LSTM(
            self.dim_lstm1_layer, self.dim_lstm2_layer,
            batch_first=True
        )
        self.output1 = nn.Linear(
            self.dim_lstm2_layer, self.dim_output1_layer
        )
        # 最集段の出力を受け取って全結合してsoftmaxに食わせるための１層のネットワーク
        self.output2 = nn.Linear(
            self.dim_output1_layer, self.output_size
        )
        # softmaxのLog版。dim=0で列、dim=1で行方向を確率変換。
        self.softmax = nn.LogSoftmax(dim=1)

    # 順伝播処理はforward関数に記載
    def forward(self, sentence_vect_list):
        # sentence_vect_listはlistで、それぞれの要素は
        # (単語数 x 単語ベクトル次元)のndarray。
        # * 単語数が文により可変な点に注意が必要。 *

        batch_size = len(sentence_vect_list)
        sentence_tensor_list = [
            torch.from_numpy(x).to(self.device) for x in sentence_vect_list
        ]
        sentence_length_list = [x.shape[0] for x in sentence_vect_list]
        sentence_length_tensor = torch.tensor(
            sentence_length_list).to(self.device)

        # バッチ処理できるように色々する
        x = pad_sequence(
            sentence_tensor_list,
            padding_value=self.padding_idx,
            batch_first=True
        )
        x = pack_padded_sequence(
            x, sentence_length_tensor,
            enforce_sorted=False,
            batch_first=True
        )

        # LSTM1層目への入力
        out, (h_n, c_n) = self.lstm1(x)
        x = out

        # LSTM2層目への入力
        out, (h_n, c_n) = self.lstm2(out)
        x = h_n.view(batch_size, -1, self.dim_lstm2_layer)

        # パーセプトロン1層目への入力
        x = F.dropout(x, p=self.output_dropout_rate, training=self.training)
        x = F.relu(self.output1(x))

        # パーセプトロン2層目への入力（ここはDropoutしない）
        x = self.output2(x)
        x = x.view(x.shape[0], x.shape[2])

        # softmaxに食わせて、確率として表現
        x = self.softmax(x)

        return x
