import os
from matplotlib import pyplot as plt
import torch
import numpy as np
import pandas as pd
import sys
import logging
import re
import MeCab
import gensim
from typing import List
import requests
import tarfile
import shutil

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

logformat_stdout = "%(asctime)s %(levelname)s %(name)s :%(message)s"

handler_stdout = logging.StreamHandler(sys.stdout)
handler_stdout.setLevel(logging.INFO)
handler_stdout.setFormatter(logging.Formatter(logformat_stdout))

logger.addHandler(handler_stdout)

logger.debug("** stdout encoding: {} **".format(sys.stdout.encoding))


def ptvsd_attach():
    import sys
    if (len(sys.argv) > 1) and (sys.argv[1] == "debug"):
        logger.debug("** Debug mode **")
        import ptvsd
        logger.info("** Waiting for attach ... **")
        handler_stdout.flush()

        ptvsd.enable_attach(address=('0.0.0.0', 5678))
        ptvsd.wait_for_attach()
        ptvsd.break_into_debugger()


class MecabInterface:
    def __init__(self):
        self.logger = logger.getChild(self.__class__.__name__)
        try:
            self.mecab = MeCab.Tagger(
                '-d /usr/lib/mecab/dic/mecab-ipadic-neologd')
            self.logger.info("** MeCab load NEologd dict: OK **")
        except Exception:
            self.mecab = MeCab.Tagger()
            self.logger.warning("** MeCab load NEologd dict failed **")
        self.logger.info("** MeCab dictionaly loaded **")

    def parse(self, text: str) -> List[List[str]]:
        cleansed_text = ' '.join(text.split())
        raw_parsed_str = self.mecab.parse(cleansed_text)
        items = (
            re.split('[\t,]', line)
            for line in raw_parsed_str.split('\n')
        )
        return list(items)


class Word2vecInterface:
    def __init__(self):
        # 東北大学の日本語ベクトルデータファイルを使用
        # www.cl.ecei.tohoku.ac.jp/~m-suzuki/jawiki_vector/data/20170201.tar.bz2
        self.logger = logger.getChild(self.__class__.__name__)
        self.workdir = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "word2vec_dict/"
        )

        if not os.path.isdir(self.workdir):
            os.makedirs(self.workdir)

        # 辞書ファイルがなかった場合にダウンロードしてきて展開する
        wordvector_downloadurl = \
            r"http://www.cl.ecei.tohoku.ac.jp/~m-suzuki/jawiki_vector/data/20170201.tar.bz2"
        wordvector_tmpfilepath = os.path.join(
            self.workdir, r'20170201.tar.bz2')
        wordvector_filepath = os.path.join(
            self.workdir, r'entity_vector.model.bin')

        if not os.path.isfile(wordvector_filepath):
            self.logger.info(
                "Start trying to extract WordVector file."
            )
            try:
                tarfile.open(wordvector_tmpfilepath).extractall(
                    path=self.workdir)
            except Exception:
                if not os.path.isfile(wordvector_tmpfilepath):
                    self.logger.info(
                        "WordVector file not found locally. Start trying to downlaod."
                    )
                    res = requests.get(wordvector_downloadurl, stream=True)
                    with open(wordvector_tmpfilepath, 'wb') as file:
                        for chunk in res.iter_content(chunk_size=1024):
                            file.write(chunk)
                tarfile.open(wordvector_tmpfilepath).extractall(
                    path=self.workdir)
            # 展開したファイルから必要なファイルだけを取り出す。
            shutil.copy(
                os.path.join(
                    self.workdir, "entity_vector/", "entity_vector.model.bin"),
                os.path.join(
                    self.workdir, "entity_vector.model.bin")
            )

            # 不要な一時ファイルを削除する。
            if os.path.isdir(os.path.join(self.workdir, "entity_vector/")):
                shutil.rmtree(os.path.join(self.workdir, "entity_vector/"))
            if os.path.isfile(wordvector_tmpfilepath):
                os.remove(wordvector_tmpfilepath)

            self.logger.info(
                "Completed: Trying to extract WordVector file."
            )

        # 辞書ファイルをメモリ上に展開する。
        self.logger.info(
            "Start to Word2Vec dictionaly loading."
        )
        self.model = gensim.models.KeyedVectors.load_word2vec_format(
            wordvector_filepath,
            binary=True)

        # 正規化を行う（-1～1の範囲に収める。LSTMでパディングにこの範囲外を使うので必要。）
        self.model.init_sims()

        self.logger.info("Completed: Word2Vec dictionaly loading.")

    def vectorize(self, text: str):
        return self.model.word_vec(text, use_norm=True)
