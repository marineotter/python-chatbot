from .common import logger, MecabInterface, Word2vecInterface
from .network import Predictor
import pandas as pd
import re
import numpy as np
import torch
from matplotlib import pyplot as plt
import os
from sklearn.utils import shuffle
import time
import datetime

from django.conf import settings


class BotModel:
    def __init__(
            self,
            mecab: MecabInterface, w2v: Word2vecInterface, model_filename: str
    ):
        self.logger = logger.getChild(self.__class__.__name__)

        # 分かち書きのためのMeCabと単語ベクトル化のためのWord2Vecのインタフェースクラスを初期化する。
        self.mecab = mecab
        self.w2v = w2v

        # 保存先のファイル名
        self.model_filename = model_filename

        self.losses_list = []

    def vectorize_text(self, text):
        cleansed_text = " ".join(text.split())
        parsed_line = self.mecab.parse(cleansed_text)
        result_vector = []
        for item in parsed_line:
            token = item[0]
            if token == "":
                continue
            try:
                result_vector.append(self.w2v.vectorize(token))
            except KeyError:
                self.logger.warning(
                    "** UNKNOWN TOKEN: \"{}\" **".format(token))
        result_vector = np.array(result_vector)
        return result_vector

    # train_x_dfには 列"content"と"answer_index"が存在することを期待している。
    # train_y_dfには 列"index"と"content"が存在することを期待している。
    # train_x_df の answer_indexと train_y_df の index が対応している。

    def load(
            self, train_x_df, train_y_df,
            epoch=500, batch_size=5,
            reset=False, autotrain=False, autosave=True):

        self.predictor = None

        self.train_x_df = train_x_df
        self.train_y_df = train_y_df

        # 分類器の正解ラベル用のone-hot展開を行う
        self.train_x_df_dummy = pd.get_dummies(
            self.train_x_df, columns=['answer_index', ])
        self.train_label_array = self.train_x_df_dummy[
            filter(
                lambda x: re.match(r'answer_index_\d+', x) is not None,
                self.train_x_df_dummy.columns)
        ].to_numpy()

        # one_hot展開したあとにもう一回indexに戻す。
        self.train_x_df["dummy_index"] = np.array(
            [np.where(x == 1)[0] for x in self.train_label_array])

        # もとのindexとdummy indexを紐付けておく。
        self.index_convert_df = self.train_x_df[[
            "answer_index", "dummy_index"]].drop_duplicates()

        # 分類機の入力を可変長ベクトルへ展開する。
        self.train_data_array = []
        for i, line in enumerate(self.train_x_df['content']):
            self.train_data_array.append(self.vectorize_text(line))

        # 正解ラベルはnumpyに展開しておく
        self.label_data_array = \
            self.train_x_df["dummy_index"].to_numpy().astype(np.int32)

        # PyTorch 分類器の定義

        try:
            wordvect_size = self.train_data_array[0].shape[1]
            tagset_size = self.train_label_array.shape[1]
        except IndexError:
            return False

        self.predictor = Predictor(
            wordvect_size=wordvect_size,
            tagset_size=tagset_size)

        if reset:
            try:
                os.remove(self.model_filename)
            except FileNotFoundError:
                pass

        try:
            self.predictor.load_state_dict(torch.load(self.model_filename))
        except Exception:
            if autotrain:
                self.train(
                    epoch=epoch, batch_size=batch_size,
                    autosave=autosave)
            else:
                return False

        # 分類器を予測モードに切り替える（学習モードを出る）
        self.predictor.eval()
        return True

    def train(self, epoch=500, batch_size=5, autosave=True):
        # PyTorch 学習の準備
        if not self.predictor:
            self.logger.warn("Train failed: Model is not ready.")
            return False

        # 分類器を学習モードに切り替える
        self.predictor.train()

        self.logger.info(
            "Start: Training (Target epoch: {} Batch size: {})".format(
                epoch, batch_size
            )
        )

        start_time = time.time()

        criterion = torch.nn.NLLLoss()
        optimizer = torch.optim.Adam(self.predictor.parameters(), lr=0.01)
        losses = []

        # PyTorch 学習（epoch数は一旦適当）
        for current_epoch in range(epoch):
            all_loss = 0
            # バッチでまとめる
            train_data_batch_list = []
            label_data_batch_list = []
            train_data_shuffled, label_data_shuffled = shuffle(
                self.train_data_array, self.label_data_array
            )
            for i in range(0, len(train_data_shuffled), batch_size):
                train_data_batch_list.append(
                    train_data_shuffled[i:i+batch_size])
                label_data_batch_list.append(
                    label_data_shuffled[i:i+batch_size])

            for train_data_batch, label_data_batch in zip(
                    train_data_batch_list, label_data_batch_list):
                # モデルが持ってる勾配の情報をリセット
                self.predictor.zero_grad()

                # train_data_batch_tensor = torch.tensor(train_data_batch)
                # label_data_batch_tensor = torch.tensor(label_data_batch)

                # 順伝播の結果を受け取る
                out = self.predictor(train_data_batch)
                # 正解とのlossを計算（せっかくdummy変数展開したが、交差エントロピー関数への入力は単数値しか無理。再変換。
                loss = criterion(
                    out, torch.tensor(label_data_batch).type(torch.LongTensor)
                )
                # 勾配をセット
                loss.backward()
                # 逆伝播でパラメータ更新
                optimizer.step()
                # lossを集計
                all_loss += loss.item()
            losses.append(all_loss)
            self.logger.info(
                "Training: Calculating... (Epoch: {} Loss: {})".format(
                    current_epoch, all_loss
                )
            )

        tmp = [losses]
        tmp.extend(self.losses_list)
        self.losses_list = tmp[:3][::-1]

        plt.clf()
        for i, losses in enumerate(self.losses_list):
            if i == 0:
                plt.plot(losses, label="Last training")
            elif i == 1:
                plt.plot(losses, label="1 trainings before")
            elif i == 2:
                plt.plot(losses, label="2 trainings before")
            else:
                pass
        plt.legend()
        plt.savefig(os.path.join(settings.BASE_DIR, "losses.png"))

        if autosave:
            self.save()

        # 分類器を予測モードに切り替える（学習モードを出る）
        self.predictor.eval()

        end_time = time.time()
        elapsed_time = end_time - start_time
        self.logger.info(
            "Completed: Training (Elapsed time: {})".format(
                str(datetime.timedelta(seconds=elapsed_time))
            )
        )
        self.logger.info(
            "Completed: Training (Target epoch: {} Batch size: {})".format(
                epoch, batch_size
            )
        )

        return True

    def save(self):
        if self.predictor:
            torch.save(self.predictor.state_dict(), self.model_filename)
            self.logger.info("** save done. **")
            return True
        else:
            self.logger.warn("Save failed: Model is not ready.")
            return False

    def predict(self, text):
        if not self.predictor:
            self.logger.warn("Predict failed: Model is not ready.")
            return False

        # 分類器を予測モードに切り替える（学習モードを出る）
        self.predictor.eval()

        result_tensor = self.predictor([self.vectorize_text(text)])

        return self.train_y_df.set_index("index").loc[
            self.index_convert_df.set_index("dummy_index").loc[
                int(torch.max(result_tensor, 1)[1][0])
            ]["answer_index"]
        ]["content"]
        return True
