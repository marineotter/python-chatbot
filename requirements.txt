# Other than PyTorch and ptvsd
mecab-python3; sys_platform != 'win32'
mecab-python-windows; sys_platform == 'win32'
gensim
django>=3.0
djangorestframework
django-bootstrap4
pandas
tqdm
matplotlib
scikit-learn