## python-chatbot

### 概要
質問に対する回答を予測する予測器です。

深層学習を行うRNNを用いた学習器に、Webインタフェースをくっつけたものとなっています。

### 使い方

#### 準備
Python3.7以降が使用可能な環境が必要です。

環境にMeCabを入れておいてください。

1. （推奨）cloneしたプロジェクトディレクトリ（以下プロジェクトルート）でvenv環境を作成してください。
2. 環境にあったpytorch 1.3以降をインストールします。（公式ページにあるpip使用方法を参考に）
3. `pip install -r requirements.txt` によりその他の依存関係を入れます。
4. Djangoの準備をします。（`py manage.py migration`など）

#### 実行
シェルを2つ用意し、以下のコマンドを別々に走らせます。

* py manage.py runserver
* py manage.py runai

ブラウザで http://localhost:8000 にアクセスしてください。（runserverの値でカスタム可能です）

**注意** 標準でDebug=Trueにしたままです。公開する前に手を入れてください。また、secret keyを書き換えてください。

#### 初期データの投入
手動で入れる方法しか用意していません。（インポート/エクスポートは今後の課題です）

1. http://localhost:8000 から質問データを投入します。
2. http://localhost:8000/data/ から回答データを投入します。
3. http://localhost:8000/data/ から質問データを学習データに投入します。
4. http://localhost:8000/data/ から学習器を学習させます。

### 解説

#### runaiコマンドについて
-hオプションでヘルプを出して参照してください。

### w2v エンティティベクトルについて
本プロジェクトではデフォルトでWord2Vecのエンティティベクトルに[東北大学の鈴木正敏先生作のエンティティベクトル](http://www.cl.ecei.tohoku.ac.jp/~m-suzuki/jawiki_vector/)を使用します。  
![クリエイティブ・コモンズ 表示 - 継承 4.0 国際ライセンス](https://licensebuttons.net/l/by-sa/4.0/88x31.png)